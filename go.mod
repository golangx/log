module gitee.com/golangx/log

go 1.14

require (
	go.uber.org/zap v1.14.1
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
)
